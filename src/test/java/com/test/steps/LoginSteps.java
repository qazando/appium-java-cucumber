package com.test.steps;
import com.test.screen.ScreenLogin;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;

public class LoginSteps {

    ScreenLogin login;

    public LoginSteps() {
        login = new ScreenLogin();
    }

    @Dado("^que eu abra o aplicativo$")
    public void que_eu_abra_o_aplicativo() {
        login.validarHomeLogin();
    }

    @Dado("^que faça login utilizando: \"([^\"]*)\" Senha: \"([^\"]*)\"$")
    public void que_faça_login_utilizando_Senha_e_Token(String email, String senha) {
        login.logarpassandostring(email, senha);
    }

    @Então("^estou logado no app$")
    public void estou_logado_no_app() {
        login.validarlogado();
    }

    @Dado("^que faça login no aplicativo$")
    public void que_faça_login_no_aplicativo() {
        login.logar();
    }

    @Quando("^eu clicar em logar$")
    public void eu_clicar_em_logar() {
    login.clicarLogar();
    }
}