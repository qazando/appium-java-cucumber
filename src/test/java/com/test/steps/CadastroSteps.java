package com.test.steps;

import com.test.screen.ScreenCadastro;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;

public class CadastroSteps {

    ScreenCadastro cadastro;

    public CadastroSteps() {
        cadastro = new ScreenCadastro();
    }

    @Quando("^eu acessar a tela de cadastro$")
    public void eu_acessar_a_tela_de_cadastro() {
        cadastro.acessarcadastro();
    }

    @Então("^vou visualizar a tela de cadastro$")
    public void vou_visualizar_a_tela_de_cadastro() {
        cadastro.validarBtnContinueCadastro();
    }
}