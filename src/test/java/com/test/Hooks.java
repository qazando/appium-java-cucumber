package com.test;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class Hooks {

    private static AppiumDriver<?> Driver;

    public static String local = "";
    public static String platform = "";

    public static AppiumDriver<?> validardriver() {

        local = System.getProperty("local").toLowerCase();
        platform = System.getProperty("platform").toLowerCase();

        if (Driver == null) {

            try {
                DesiredCapabilities caps = new DesiredCapabilities();

                if (local.equals("devicefarm") && platform.equals("android")) {

                    // Set your access credentials
                    caps.setCapability("browserstack.user", "seuuser");
                    caps.setCapability("browserstack.key", "suakey");

                    // Set URL of the application under test
                    caps.setCapability("app", "bs://seu BS");

                    // Specify device and os_version for testing
                    caps.setCapability("device", "Google Pixel 3");
                    caps.setCapability("os_version", "9.0");

                    Driver = new AndroidDriver<>(new URL("http://hub.browserstack.com/wd/hub"), caps);
                } else if (local.equals("devicefarm") && platform.equals("ios")) {

                    // Set your access credentials
                    caps.setCapability("browserstack.user", "seuuser");
                    caps.setCapability("browserstack.key", "suakey");

                    // Set URL of the application under test
                    caps.setCapability("app", "bs://seu BS");

                    // Specify device and os_version for testing
                    caps.setCapability("device", "Google Pixel 3");
                    caps.setCapability("os_version", "9.0");

                    Driver = new AndroidDriver<>(new URL("http://hub.browserstack.com/wd/hub"), caps);
                } else if (local.equals("local") && platform.equals("android")) {
                    System.out.println("Pegou Local e Android");

                    caps.setCapability("app", new File("apps/app-debug.apk"));
                    caps.setCapability("deviceName", "emulator-5524");
                    caps.setCapability("version", "9.0");
                    caps.setCapability("platformName", "Android");
                    Driver = new AndroidDriver(new URL("http://localhost:4723/wd/hub"), caps);
                } else if (local.equals("local") && platform.equals("ios")) {
                    System.out.println("Pegou Local e IOS");

                    caps.setCapability("app", new File("apps/LoginExample.app"));
                    caps.setCapability("deviceName", "iPhone 13");
                    caps.setCapability("platformName", "iOS");
                    caps.setCapability("platformVersion", "15.0");
                    caps.setCapability("automationName", "XCUITest");
                    Driver = new IOSDriver(new URL("http://localhost:4723/wd/hub"), caps);
                } else {
                    System.out.println("Precisa passar o Platform e Local onde os testes vão executar");
                }
            } catch (IllegalArgumentException | MalformedURLException e) {
                System.out.println(" ==== Alguma coisa deu Errado nas capabilities ==== ");
                System.exit(1);
                return Driver;
            }
        }
        return Driver;
    }

    public static AppiumDriver<?> getDriver() {
        return Driver;
    }

    public static void quitDriver() {
        if (Driver != null) {
            Driver.quit();
        }
    }

    @Before
    public static void iniciarNovamente() {
        if (getDriver() != null) {
            getDriver().launchApp();
        } else {
            validardriver();
        }
    }

    @After
    public void fecharApp() {
        if (getDriver() != null) {
            getDriver().closeApp();
        }
    }
}