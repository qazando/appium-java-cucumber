package com.test.screen;

import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.remote.RemoteWebElement;

public class ScreenLogin extends BaseScreen {

    @AndroidFindBy(id = "login_username")
    @iOSFindBy(accessibility = "login_username")
    private RemoteWebElement campoemail;

    @AndroidFindBy(id = "login_password")
    @iOSFindBy(accessibility = "login_password")
    private RemoteWebElement camposenha;

    @AndroidFindBy(id = "login_button")
    @iOSFindBy(accessibility = "login_button")
    private RemoteWebElement botaologar;

    @AndroidFindBy(id = "mensagem2")
    @iOSFindBy(accessibility = "login_button")
    private RemoteWebElement textologado;

    public void logar() {
        campoemail.sendKeys("qazando@gmail.com");
        camposenha.sendKeys("1234");
    }

    public void logarpassandostring(String email, String senha) {
        campoemail.sendKeys(email);
        camposenha.sendKeys(senha);
        botaologar.click();
    }

    public void validarlogado() {
        textologado.isDisplayed();
    }

    public void clicarLogar() {
        botaologar.click();
    }

    public void validarHomeLogin() {
        campoemail.isDisplayed();
    }
}