package com.test.screen;

import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.remote.RemoteWebElement;

public class ScreenCadastro extends BaseScreen {

    @AndroidFindBy(id = "btn_sign_up_login")
    @iOSFindBy(accessibility = "btn_sign_up_login")
    private RemoteWebElement botaoadastrologin;

    @AndroidFindBy(id = "btn_continue_sign_up")
    @iOSFindBy(accessibility = "btn_continue_sign_up")
    private RemoteWebElement botaocontinuarcadastro;

    public void acessarcadastro() {
        botaoadastrologin.click();
    }

    public void validarBtnContinueCadastro() {
        botaocontinuarcadastro.isDisplayed();
    }
}