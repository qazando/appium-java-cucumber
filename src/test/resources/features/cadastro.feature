#language: pt

@cadastro @e2e
Funcionalidade: Cadastro de usuario

  @cadastro-usuario
  Cenário: Validar tela de cadastro
    Dado que eu abra o aplicativo
    Quando eu acessar a tela de cadastro
    Então vou visualizar a tela de cadastro