#language: pt

@login @e2e
Funcionalidade: Login

  @login-tabela
  Esquema do Cenário: Realizar login utilizando Tabela
    Dado que faça login utilizando: "<usuario>" Senha: "<senha>"
    Então estou logado no app

    Exemplos:'
      | usuario             | senha  |
      | qazando@gmail.com   | 123456 |

  @login-direto
  Cenário: Realizar login passando os parametros diretamente
    Dado que faça login utilizando: "qazando@gmail.com" Senha: "234567"
    Então estou logado no app

  @login-simples
  Cenário: Realizar login passando os parametros diretamente
    Dado que faça login no aplicativo
    Quando eu clicar em logar
    Então estou logado no app